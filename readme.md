# Vagrant Builder Introduction
## What is this 
This is a tool which allows you to create a vagrant virtual machine with the following stack:

* Ubuntu 14.04
* PHP 7
    * With xDebug
* nginx
* MariaDB 10.1
* (Optionally) One of 
    * Wordpress
    * Laravel


##  What is this not
This is not a sample vagrant file, like, for example, the excellent [Vaprobash](https://github.com/fideloper/Vaprobash) project.

This is also fairly opinionated. It is not the intention that the system supports every possible PHP project, or every Virtual Machine system but only what The Tomorrow Lab use often.

## Getting Started

### Prerequisites
* You will need
    * [Composer](https://getcomposer.org/)
    * [Virtual Box](https://www.virtualbox.org/)
    * [Vagrant](https://www.vagrantup.com/)
    * The Following Vagrant Plugins
        * [vagrant-hostmanager](https://github.com/devopsgroup-io/vagrant-hostmanager)
        * [vagrant-auto network](https://github.com/oscar-stack/vagrant-auto_network)
    * A directory called "dev-certs" at the same level as the main file share for your site. 
        * e.g. If you want to run your main site at /var/www/mysite.dev then you must have a directory at /var/www/dev-certs

### Installing the vagrant builder (Do this once)
* Open a Shell Window
* Clone this repository
* cd into the directory the cloned repository is in
* Run `composer install`

### Building a new site environment
* From the parent directory of your vagrant builder project
    * Run one of
        *  `php ./vagrant-builder/bin/vagrant-box-builder LaravelBox`
        *  `php ./vagrant-builder/bin/vagrant-box-builder WordpressBox`
        *  `php ./vagrant-builder/bin/vagrant-box-builder ExistingSiteBox`
    * The system will then ask you to answer questions, so that you can build your box. Based on the supplied questions you'll get a nice new Vagrant VM of the specification you require.
    * You won't need to change your hosts file!
    * You'll then have a running vagrant box.
    * Try visiting the domain you specified.
        * You will be redirected to the https:// version
        * Then warned about an invalid certificate
        * You will find the certificate you need in the dev-certs directory. So grab it, add it to your Keychain Access (or equivalent) and approve it.
        * You can now access the site over https, without having to add an exception for the domain.


## xDebug
[xDebug](https://xdebug.org/) is a marvellous tool for debugging PHP projects. The Vagrant builder installs xdebug on your virtual machine automatically.
Once you have created a new Vagrant VM, you will also easily be able to easily switch xdebug on and off by sshing into the box, then running one of:

* xdebug-on
* xdebug-off
* xdebug-autostart

In order to actually use xdebug, you'll need to consult the documentation for your IDE.

## Future Development
* Create a phar file so that the script can easily be installed globally.
* Use packer so that provisioning takes less time in future.
* Make the system self-updating.
* Make sure that samba is supported on windows.

## Pull Requests
* Pull requests are very welcome on this project. We may not add in support for every framework and plugin, however we will consider them. 
* Pull requests which solve any problems in Future Development are very welcome indeed.

### Security
If you spot a security issue with this system, please email ryan@thetomorrowlab.com directly, rather than submit a pull request.
