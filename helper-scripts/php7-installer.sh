#!/usr/bin/env bash

sudo apt-get update
sudo add-apt-repository ppa:ondrej/php -y

sudo apt-get upgrade
sudo apt-get update
sudo apt-get install php7.0 -y
sudo apt-get install php7.0-cgi -y
sudo apt-get install php7.0-cli -y
sudo apt-get install php7.0-fpm -y
sudo apt-get install php7.0-dev -y
sudo apt-get install php7.0-curl -y
sudo apt-get install php7.0-gd -y
sudo apt-get install php7.0-intl -y
sudo apt-get install php7.0-mcrypt -y
sudo apt-get install php7.0-readline -y
sudo apt-get install php7.0-odbc -y
sudo apt-get install php7.0-recode -y
sudo apt-get install php7.0-xmlrpc -y
sudo apt-get install php7.0-xsl -y
sudo apt-get install php7.0-json -y
sudo apt-get install php7.0-sqlite3 -y
sudo apt-get install php7.0-mysql -y
sudo apt-get install php7.0-opcache -y
sudo apt-get install php7.0-bz2 -y
sudo apt-get install php7.0-bcmath -y
sudo apt-get install php7.0-mbstring -y
sudo apt-get install php7.0-soap -y
sudo apt-get install php7.0-xml -y
sudo apt-get install php7.0-zip -y
