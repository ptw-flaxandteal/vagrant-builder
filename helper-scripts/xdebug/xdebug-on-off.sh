#!/usr/bin/env bash

function xdebug-on {
    sudo cp /vagrant/helper-scripts/xdebug/90-xdebug-on.ini /etc/php/7.0/fpm/conf.d/xdebug.ini
    sudo service php7.0-fpm restart
}

function xdebug-off {
   sudo cp /vagrant/helper-scripts/xdebug/90-xdebug-off.ini /etc/php/7.0/fpm/conf.d/xdebug.ini
   sudo service php7.0-fpm restart
}

function xdebug-autostart {
    sudo cp /vagrant/helper-scripts/xdebug/90-xdebug-autostart.ini /etc/php/7.0/fpm/conf.d/xdebug.ini
    sudo service php7.0-fpm restart
}