#!/usr/bin/env bash

sudo apt-get update
sudo apt-get install nginx -y
sudo update-rc.d nginx enable
sudo service nginx start