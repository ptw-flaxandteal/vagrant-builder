#!/usr/bin/env bash
if [[ ! -f "/vagrant/site/composer.json" ]]; then
    cd /vagrant
    mkdir site
    sudo git clone --depth 1  https://github.com/roots/bedrock.git site
    rm site/.git -R
    cd /vagrant/site
    composer install
    mkdir /vagrant/site/web/app/themes -p
    cd /vagrant/site/web/app/themes
    git clone --depth 1 https://JamieBrennan@bitbucket.org/thetomorrowlab/tomorrowpress.git TomorrowPress
    rm tomorrowpress/.git -R
    cp /vagrant/helper-scripts/wordpress-cli.config.yaml /vagrant/wp-cli.local.yml
    sed "s/{{DOMAIN}}/$1/g"  /vagrant/helper-scripts/wordpress.env > /vagrant/site/.env

    AUTH_KEY="$(openssl rand -hex 32)";
    SECURE_AUTH_KEY="$(openssl rand -hex 32)";
    LOGGED_IN_KEY="$(openssl rand -hex 32)";
    NONCE_KEY="$(openssl rand -hex 32)";
    NONCE_SALT="$(openssl rand -hex 32)";
    AUTH_SALT="$(openssl rand -hex 32)";
    SECURE_AUTH_SALT="$(openssl rand -hex 32)";
    LOGGED_IN_SALT="$(openssl rand -hex 32)";

    sed -i "s/{{AUTH_KEY}}/$AUTH_KEY/g" /vagrant/site/.env
    sed -i "s/{{SECURE_AUTH_KEY}}/$SECURE_AUTH_KEY/g" /vagrant/site/.env
    sed -i "s/{{LOGGED_IN_KEY}}/$LOGGED_IN_KEY/g" /vagrant/site/.env
    sed -i "s/{{NONCE_KEY}}/$NONCE_KEY/g" /vagrant/site/.env
    sed -i "s/{{AUTH_SALT}}/$AUTH_SALT/g" /vagrant/site/.env
    sed -i "s/{{SECURE_AUTH_SALT}}/$SECURE_AUTH_SALT/g" /vagrant/site/.env
    sed -i "s/{{LOGGED_IN_SALT}}/$LOGGED_IN_SALT/g" /vagrant/site/.env
    sed -i "s/{{NONCE_SALT}}/$NONCE_SALT/g" /vagrant/site/.env

    rm /vagrant/site/web/wp/wp-content/themes -R
    cd /vagrant/site/
    composer require wpackagist-plugin/wordfence
    composer require wpackagist-plugin/ewww-image-optimizer
    composer require wpackagist-plugin/wordpress-seo
    composer require wpackagist-plugin/wp-fastest-cache
    composer require wpackagist-plugin/easy-wp-smtp
    sed -i -e '$a\' /vagrant/site/config/application.php
    echo "define('WP_DEFAULT_THEME', env('WP_DEFAULT_THEME'));" >> /vagrant/site/config/application.php
fi;
