#!/usr/bin/env bash

echo "Installing MariaDB"
sudo apt-get install software-properties-common -y
sudo apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xcbcb082a1bb943db
sudo add-apt-repository 'deb [arch=amd64,i386] http://lon1.mirrors.digitalocean.com/mariadb/repo/10.1/ubuntu trusty main'
sudo apt-get update -y

echo "SETTING UP MariaDB PREFS"
debconf-set-selections <<< "maria-db-10.1 mysql-server/root_password password root"
debconf-set-selections <<< "maria-db-10.1 mysql-server/root_password_again password root"

sudo apt-get install -qq mariadb-server

mysql -u root -proot -Bse "CREATE DATABASE IF NOT EXISTS site"
