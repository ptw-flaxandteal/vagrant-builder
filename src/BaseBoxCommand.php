<?php
namespace VagrantBoxBuilder;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class BaseBoxCommand extends Command
{
    protected $location;
    protected $answers;

    protected function configure()
    {
        $this->setName('BaseBox')
            ->setDescription('Builds your base box');
    }


    protected function getQuestions()
    {
        $questions = [];
        $questions['hostname'] = new Question('What host name should your base box have? ');
        $questions['directory'] = new Question('Where should your base box be ? ', getcwd());
        $questions['cores'] = new Question('How many cores should your base box have? ', 1);
        $questions['memorySize'] = new Question('How much memory should your base box have? ', 1024);
        return $questions;
    }

    protected function askQuestions(InputInterface $input, OutputInterface $output)
    {
        $replacements = [];
        foreach($this->getQuestions() as $questionKey => $question) {
            $qh = new QuestionHelper();
            $replacements[$questionKey] = $qh->ask($input, $output, $question);
        }

        return $replacements;
    }

    protected function getReplacements() {
        $replacements = [];
        $replacements['scripts'] = '';
        return $replacements;
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $this->answers = $this->askQuestions($input, $output);

        $replacements = $this->answers;
        $replacements += $this->getReplacements();

        $preparedReplacements = [];

        foreach($replacements as $key => $replacement) {
            $preparedReplacements['{{'.$key.'}}'] = $replacement;
        }

        $directory = $this->answers['directory'].'/'.$this->answers['hostname'];
        $output->writeln('Writing vagrant file for <info>'.$this->answers['hostname'].'</info> into <info>'.$this->answers['directory'].'</info>');

        $vagrantFileContents = file_get_contents(__DIR__.'/emptyVagrantFile.txt');
        $vagrantFileContents = str_replace(array_keys($preparedReplacements), array_values($preparedReplacements), $vagrantFileContents);
        mkdir($directory);
        mkdir($directory.'/helper-scripts');
        exec('cp -r '.dirname(__DIR__).'/helper-scripts'.' '.$directory);
        if(file_exists($directory.'/Vagrantfile')) {
            $output->writeln('<error>Vagrant file exists!</error>');
            exit();
        }

        file_put_contents($directory.'/Vagrantfile', $vagrantFileContents);

        passthru('cd '.$directory.'; vagrant up');
        $output->writeln('If everything worked, your box should be running at <info>'.$this->answers['hostname'].'</info>');
    }


    protected function buildBaseBox($fileLocation, $hostName) {
        $commands = [];
        $commands[] = 'cd '.$fileLocation;

    }
}