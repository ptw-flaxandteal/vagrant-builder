<?php

namespace VagrantBoxBuilder;


use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class WordpressBoxCommand extends BaseBoxCommand
{
    protected function configure()
    {
        $this->setName('WordpressBox')
            ->setDescription('Builds you a box with a base wordpress install on it');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        parent::execute($input, $output);

    }

    protected function getReplacements() {
        $replacements = [];
        $scripts = [];
        $scripts[] = 'config.vm.provision "shell", path: "helper-scripts/add-gitignore.sh"';
        $scripts[] = 'config.vm.provision "shell", path: "helper-scripts/install-git.sh"';
        $scripts[] = 'config.vm.provision "shell", path: "helper-scripts/install-composer.sh"';
        $scripts[] = 'config.vm.provision "shell", path: "helper-scripts/nginx-add-host.php", args: "'.$this->answers['hostname'].' /vagrant/site/web"';
        $scripts[] = 'config.vm.provision "shell", path: "helper-scripts/install-wp-cli.sh", args: "http://'.$this->answers['hostname'].'"';
        $scripts[] = 'config.vm.provision "shell", path: "helper-scripts/install-wordpress.sh", args: "'.$this->answers['hostname'].'" ';
        $scripts[] = 'config.vm.provision "shell", path: "helper-scripts/make-wordpress-env-file.sh", args: "'.$this->answers['hostname'].'" ';
        $scripts[] = 'config.vm.provision "shell", path: "helper-scripts/xdebug/install-xdebug.sh"';
        $replacements['scripts'] = implode("\n", $scripts);
        return $replacements;
    }

}